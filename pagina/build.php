<?php
require '../config.php';

// O arquivo pode ser chamado multiplas vezes nos posts de index.php
// Caso a função seja redeclarada ocorre erro
if (!function_exists('LerArquivo')) {
  function LerArquivo($filename){
    $handle = fopen($filename, "r");
    $contents = fread($handle, filesize($filename));
    fclose($handle);
    return $contents;
  }
  function RoleplayUglyfied(){
    $contents = LerArquivo(ROLEPLAY);

    // Substituir cada quebra de linha com <br>
    $contentsfmt = preg_replace("/\n/",'<br/>', $contents);
    // Remover cada simbolo de fala
    $contentsfmt = str_replace("$#", "<div class='titlebar'><span class='titulo'>", $contentsfmt);
    $contentsfmt = str_replace("#$", "</span><hr></div>", $contentsfmt);
    $contentsfmt = str_replace("$1", "<span class='fala1'>", $contentsfmt);
    $contentsfmt = str_replace("$2", "<span class='fala2'>", $contentsfmt);
    $contentsfmt = str_replace("$3", "<span class='fala3'>", $contentsfmt);
    $contentsfmt = str_replace("$4", "<span class='fala4'>", $contentsfmt);
    $contentsfmt = str_replace("$5", "<span class='fala5'>", $contentsfmt);
    $contentsfmt = str_replace("%$", "</span>", $contentsfmt);
    return $contentsfmt;
  }

  function RoleplayCounterWords(){
    $contents = LerArquivo(ROLEPLAY);
    // Remover cada quebra de linha
    $contentsPuro = preg_replace("/\n/", '', $contents);
    // Remover titulo e marcação
    $contentsPuro = preg_replace("\$\#(.*?)\#\$", '', $contentsPuro);
    // Remover cada simbolo de fala
    $contentsPuro = str_replace("$1", '', $contentsPuro);
    $contentsPuro = str_replace("$2", '', $contentsPuro);
    $contentsPuro = str_replace("$3", '', $contentsPuro);
    $contentsPuro = str_replace("$4", '', $contentsPuro);
    $contentsPuro = str_replace("$5", '', $contentsPuro);
    $contentsPuro = str_replace("%$", '', $contentsPuro);
    $numeroPalavras = str_word_count($contentsPuro,0);
    return $numeroPalavras;
  }

  function LegendasUglyfied(){
    $contents = LerArquivo(LEGENDAS);
    // Remover cada simbolo de fala
    $contentsfmt = str_replace("$1","<li class='legenda legenda1'>",$contents);
    $contentsfmt = str_replace("$2","<li class='legenda legenda2'>",$contentsfmt);
    $contentsfmt = str_replace("$3","<li class='legenda legenda3'>",$contentsfmt);
    $contentsfmt = str_replace("$4","<li class='legenda legenda4'>",$contentsfmt);
    $contentsfmt = str_replace("$5","<li class='legenda legenda5'>",$contentsfmt);
    $contentsfmt = str_replace("%$","</li>",$contentsfmt);
    return $contentsfmt;
  }

  function AdendosUglyfied(){
    $filename = ADENDOS;

    $handle = fopen($filename, "r");
    $contentsfmt = '';
    while (($line = fgets($handle)) !== false) {
      if ($line != '\n') {
        $contentsfmt .= "<li>" . $line . "</li>";
      }
    }
    return $contentsfmt;
  }

  function FichaUglyfied(){
    $individuos = array_diff(scandir(FICHAS), array('..', '.'));
    $contentsfmt = '';
    foreach ($individuos as $individuo) {
      $contentsfmt .= '<div class="status-container">' . $individuo . "
      <ul class='status-list'>
      <li class='status statusck'>ck" .  LerArquivo(FICHAS . DS . $individuo . DS . 'status-ck.txt') . "</li>
      <li class='status statushp'>hp" . LerArquivo(FICHAS . DS . $individuo . DS . 'status-hp.txt') . "</li>
      <li class='status statusst'>st" . LerArquivo(FICHAS . DS . $individuo . DS . 'status-st.txt') . "</li>
      </ul>
      </div>
      <br/>";
    }

    foreach ($individuos as $individuo) {
      $contentsfmt .= "[spoiler=Descritivos $individuo]
      [spoiler=Arsenal]" . ArsenalUglyfied(FICHAS . DS . $individuo . DS . 'arsenal.txt') . "[/spoiler]
      [spoiler=Itens]" . BrUglyfied(FICHAS . DS . $individuo . DS . 'itens.txt') . "[/spoiler]
      [spoiler=Jutsus]" . BrUglyfied(FICHAS . DS . $individuo . DS . 'jutsus.txt') . "[/spoiler]
      [/spoiler]";
    }

    return $contentsfmt;
  }

  function ArsenalUglyfied($filename){
    $handle = fopen($filename, "r");
    $contentsfmt = '<ul>';
    while (($line = fgets($handle)) !== false) {
      if ($line != '\n') {
        $contentsfmt .= "<li>" . $line . "</li>";
      }
    }
    $contentsfmt .= '</ul>';
    fclose($handle);
    return $contentsfmt;
  }

  function BrUglyfied($filename){
    $contents = LerArquivo($filename);
    // Substituir cada quebra de linha com <br>
    $contentsfmt = preg_replace("/\n/", '<br/>', $contents);
    return $contentsfmt;
  }

  function TemplateUglyfied(){
    $contents = LerArquivo(TEMPLATE);
    // Remover cada quebra de linha
    $contentsbr = preg_replace("/\n/", '', $contents);
    return $contentsbr;
  }

  function AllUglyfied(){
    $contentsTemplate = TemplateUglyfied();
    $contentsRoleplay = RoleplayUglyfied();
    $contentsLegendas = LegendasUglyfied();

    // Substituições de funções no arquivo escrito
    $contentsAll = str_replace("<?php require '../config.php'; require BUILD;?>", '', $contentsTemplate);
    $contentsAll = str_replace("<?php echo RoleplayUglyfied();?>", $contentsRoleplay, $contentsAll);
    $contentsAll = str_replace("<?php echo LegendasUglyfied();?>", $contentsLegendas, $contentsAll);
    $contentsAll = str_replace("<?php echo RoleplayCounterWords();?>", RoleplayCounterWords(), $contentsAll);
    $contentsAll = str_replace("<?php echo AdendosUglyfied();?>", AdendosUglyfied(), $contentsAll);
    $contentsAll = str_replace("<?php echo FichaUglyfied();?>", FichaUglyfied(), $contentsAll);

    return $contentsAll;
  }
}

// O arquivo pode ser chamado multiplas vezes
// Portanto so deve "cuspir" o build quando for acessado em /build.php
if ($_SERVER['REQUEST_URI'] == "/build.php") {
  echo htmlspecialchars(AllUglyfied());
}
?>
