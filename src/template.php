<?php require '../config.php'; require BUILD;?>
<style>
.neni-post .check-goyo:checked ~ .goyo-tela{
  font-size: 1.5rem !important;
  line-height: 1.5 !important;
  transition: 2s !important;

  /* disposição tela */
  position: fixed !important;
  top: 0 !important;
  left: 0 !important;
  width: 100% !important;
  height: 100% !important;
  z-index: 2000 !important;
  overflow-y: hidden !important;
}
.neni-post .goyo-tela{
  color: #9EA1B3 !important;
  background: #262831 !important;
  padding: 5% !important;
  transition: 2s !important;
  overflow: hidden !important;
}
.neni-post .check-goyo:checked ~ .goyo-tela > .goyo-texto{
  margin: 5% !important;
  height: 90% !important;
  overflow-y: scroll !important;
}
.neni-post .check-goyo:checked {
  visibility: visible !important;

  /* disposição tela */
  position: fixed !important;
  bottom: 0 !important;
  right: 0 !important;
  margin: 5% !important;
  z-index: 2001 !important;
}

/* Falas */
.neni-post .fala1{
  color: #4A9AE6 !important;
}
.neni-post .fala2{
  color: #DD696D !important;
}
.neni-post .fala3{
  color: #BD74CD !important;
}
.neni-post .fala4{
  color: #70AC76 !important;
}
.neni-post .fala5{
  color: #D29A62 !important;
}
.neni-post .legendas{
  margin: 0 !important;
}
.neni-post .legenda{
  color: #262831 !important;
  padding-left: 1rem !important;
  padding-right: 1rem !important;
  border-top: 2rem !important;
}
.neni-post .legenda:not(:first-child){
  padding-left: 4rem !important;
}
.neni-post .legenda0{
  background: #E0E2E4 !important;
}
.neni-post .legenda1{
  background: #4A9AE6 !important;
}
.neni-post .legenda2{
  background: #DD696D !important;
}
.neni-post .legenda3{
  background: #BD74CD !important;
}
.neni-post .legenda4{
  background: #70AC76 !important;
}
.neni-post .legenda5{
  background: #D29A62 !important;
}

/* status */
.neni-post .status-container{
  width: 100% !important;
  text-align: center !important;
}
.neni-post ul.status-list{
  margin: 0 !important;
}
.neni-post .status{
  color: #262831 !important;
  padding-left: 1rem !important;
  padding-right: 1rem !important;
  border-top: 2rem !important;
  display: inline !important;
}
.neni-post .statusck{
  background: #467299 !important;
}
.neni-post .statushp{
  background: #AC5A62 !important;
}
.neni-post .statusst{
  background: #A58E62 !important;
}

/* Titulo post */
.neni-post .titlebar{
  background: #262831 !important;
  position: sticky !important;
  top: 0 !important;
}
.neni-post .titulo{
  text-transform: uppercase !important;
}
.neni-post hr{
  border-color: #59606D !important;
}
</style>

<div class="neni-post">
  <input type="checkbox" class="check-goyo"> Goyo mode!

  <div class="goyo-tela">
    <div class="goyo-texto">

      <?php echo RoleplayUglyfied();?>

      <br/>

      <ul class="legendas">
        <li class="legenda legenda0">Falas: </li>
        <?php echo LegendasUglyfied();?>
      </ul>

      <br/>

      [spoiler=Adendo]
      <ul>
        <?php echo AdendosUglyfied();?>
      </ul>
      [/spoiler]

      <br/>

      <?php echo FichaUglyfied();?>

      <br/>

      <?php echo RoleplayCounterWords();?> Palavras...
    </div>
  </div>
</div>
